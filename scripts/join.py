#!/usr/bin/env python
import sys

import git
import numpy as np
import pandas as pd


def git_ref():
    repo = git.Repo(search_parent_directories=True)
    sha = repo.head.object.hexsha
    tagmap = {repo.commit(t): t for t in repo.tags}
    if repo.is_dirty():
        return f"{sha}+dirty"
    try:
        return tagmap[repo.commit(sha)]
    except KeyError:
        return sha


def rolling_zscore(x, window=30):
    r = x.rolling(window=window, center=True)
    mean = r.mean().shift(1)
    stdv = r.std(ddof=0).shift(1)
    score = (x - mean) / stdv
    return score.fillna(0)


def main(raw_file: str, signal_file: str = ""):
    with open(raw_file, "r") as f:
        description = f.readline().strip()
        raw_df = pd.read_csv(f)
    if signal_file:
        with open(signal_file, "r") as f:
            signal_df = pd.read_csv(f, skiprows=1)
        df = pd.merge(raw_df, signal_df, on="Temperature")
    else:
        df = raw_df
    # Ensure sorted by temperature
    df.sort_values(by="Temperature", inplace=True)
    # Find and remove outliers using Z-score
    before = len(df)
    df = df[(np.abs(rolling_zscore(df)) < 3).all(axis=1)]
    after = len(df)
    if before != after:
        print(f"removed {before-after} outliers from {description}", file=sys.stderr)
    version = f"created with rtsc {git_ref()}"
    print(",".join([description, version]))
    df["Delta V"] = pd.Series(df["Measured Voltage"]).diff().fillna(0) * 1e9
    df["Delta chi"] = pd.Series(df["Superconducting Signal"]).diff().fillna(0) * 1e9
    df["Delta Delta V"] = pd.Series(df["Delta V"]).diff().fillna(0)
    df["Delta Delta chi"] = pd.Series(df["Delta chi"]).diff().fillna(0)
    df.to_csv(sys.stdout, index=False)


if __name__ == "__main__":
    main(*sys.argv[1:])
