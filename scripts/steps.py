#!/usr/bin/env python
# Replicating the analysis from https://arxiv.org/abs/2201.07686
import sys

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from numpy.polynomial import polynomial as npp
from scipy import signal
from scipy.interpolate import UnivariateSpline

STYLE = {
    "s": 1,
}

DPI = 300


def plot(df, output_file, title, fit="spline"):
    "Scatter plot"
    plt.figure(figsize=(8, 13.5))
    ax1 = plt.subplot(311)
    plt.title(title.split(":")[1])
    plt.scatter(df["Temperature"], df["digital"], **STYLE)
    ax1.set_xlabel("T [K]")
    ax1.set_ylabel("χ [nV]")
    ax2 = plt.subplot(312)
    plt.scatter(df["Temperature"], df["analog"], **STYLE)
    ax2.set_xlabel("T [K]")
    ax2.set_ylabel("χ [nV]")
    ax3 = plt.subplot(313)
    plt.scatter(df["Temperature"], df["analog"], **STYLE)
    if fit == "poly":
        coefs = npp.polyfit(df["Temperature"], df["analog"], 5)
        plt.plot(df["Temperature"], npp.polyval(df["Temperature"], coefs), "r")
    elif fit == "spline":
        spl = UnivariateSpline(df["Temperature"], df["analog"], k=3, s=0.1)
        xmin, xmax, ymin, ymax = plt.axis()
        plt.vlines(spl.get_knots(), ymin=ymin, ymax=ymax, linestyles="dashed")
        plt.plot(df["Temperature"], spl(df["Temperature"]), "r")
    ax3.set_xlabel("T [K]")
    ax3.set_ylabel("χ [nV]")
    plt.savefig(output_file, dpi=DPI)


def offset(x, y):
    return np.where(x < 172.5, y + 0.6, y + 0.4)


def main(data_file, output_file):
    with open(data_file, "r") as f:
        description = f.readline().strip().split(",")[0]
        df = pd.read_csv(f)
    df["offset Delta chi"] = offset(df["Temperature"], df["Delta chi"] / 0.1655)
    df["wrapped Delta chi"] = df["offset Delta chi"] % 1
    df["discrete step"] = np.floor(df["offset Delta chi"])
    df["discrete step"][0] = df["Superconducting Signal"][0] * 1e9 / 0.1655
    df["digital"] = pd.Series(df["discrete step"]).cumsum() * 0.1655
    df["analog"] = df["Superconducting Signal"] * 1e9 - df["digital"]
    plot(df, output_file, description)


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
