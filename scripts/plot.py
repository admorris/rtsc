#!/usr/bin/env python
import math
import sys

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt

STYLE = {
    "s": 1,
}

DPI = 200


def central_quantile(x, nsigma=3):
    mean = np.mean(x)
    sigma = np.std(x)
    return mean - nsigma * sigma, mean + nsigma * sigma


def plot(df, output_file, title):
    plt.figure(figsize=(16, 13.5))
    ax1 = plt.subplot(321)
    plt.title(title.split(":")[1])
    plt.scatter(df["Temperature"], df["Measured Voltage"] * 1e6, **STYLE)
    ax1.set_xlabel("T [K]")
    ax1.set_ylabel("V [mV]")
    ax2 = plt.subplot(322, sharex=ax1)
    plt.scatter(df["Temperature"], df["Superconducting Signal"] * 1e9, **STYLE)
    ax2.set_xlabel("T [K]")
    ax2.set_ylabel("χ [nV]")
    ax3 = plt.subplot(323, sharex=ax1)
    plt.scatter(df["Temperature"], df["Delta V"], **STYLE)
    ax3.set_xlabel("T [K]")
    ax3.set_ylabel("ΔV [nV]")
    ax4 = plt.subplot(324, sharex=ax1)
    plt.scatter(df["Temperature"], df["Delta chi"], **STYLE)
    # ax4.set_ylim(*central_quantile(df["Delta chi"]))
    ax4.set_xlabel("T [K]")
    ax4.set_ylabel("Δχ [nV]")
    ax5 = plt.subplot(325, sharex=ax1)
    plt.scatter(df["Temperature"], np.abs(df["Delta Delta V"]), **STYLE)
    ax5.set_xlabel("T [K]")
    ax5.set_ylabel("|Δ²V| [nV]")
    ax6 = plt.subplot(326, sharex=ax1)
    plt.scatter(df["Temperature"], np.abs(df["Delta Delta chi"]), **STYLE)
    # ax6.set_ylim(*central_quantile(df["Delta Delta chi"]))
    ymax = np.percentile(np.abs(df["Delta Delta chi"]), 90)
    ax5.set_ylim(-0.1 * ymax, 1.1 * ymax)
    ax6.set_ylim(-0.1 * ymax, 1.1 * ymax)
    ax6.set_xlabel("T [K]")
    ax6.set_ylabel("|Δ²χ| [nV]")
    #    plt.tight_layout()
    plt.savefig(output_file, dpi=DPI)


def plot2(df, output_file, title):
    plt.figure(figsize=(8, 4.5))
    plt.title(title.split(":")[1])
    plt.scatter(
        df["Temperature"],
        (df["Measured Voltage"] - df["Superconducting Signal"]) * 1e6,
        **STYLE,
    )
    plt.xlabel("T [K]")
    plt.ylabel("V−χ [mV]")
    plt.savefig(output_file, dpi=DPI)


def hist(df, output_file, title):
    "Histogram"
    plt.figure(figsize=(8, 4.5))
    plt.title(title.split(":")[1])
    x_values = np.abs(df["Delta Delta chi"])
    xmax = 0.17  # np.percentile(x_values, 90)
    plt.hist(x_values, bins=1000, range=(0, xmax))
    plt.xlabel("|Δ²χ| [nV]")
    plt.savefig(output_file, dpi=DPI)


def main(data_file, output_file):
    with open(data_file, "r") as f:
        description = f.readline().strip().split(",")[0]
        df = pd.read_csv(f)
    tmin = df["Temperature"].min()
    tmax = df["Temperature"].max()
    plot(df, output_file, description)
    basename, extension = output_file.split(".")
    plot2(df, f"{basename}_background.{extension}", description)
    hist(df, f"{basename}_ddchi_histogram.{extension}", description)


if __name__ == "__main__":
    main(sys.argv[1], sys.argv[2])
