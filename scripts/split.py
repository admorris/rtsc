#!/usr/bin/env python
import math
import re
import sys
from copy import copy

DEBUG = False


def repair_float(line: str) -> float:
    "Clean up errant characters from OCR"
    line = re.sub(r"[\s|,]", "", line)
    line = re.sub(r"\.$", "", line)
    # The OCR seems to frequently mistake "E-08" for "E-03" or "E-038".
    # From a quick visual inspection, there seem to be no genuine occurrences of
    # "E-03", but if there are, I'm in trouble.
    line = re.sub(r"E-038*", "E-08", line)
    return str(float(line))


def repair_rows(lines: list[str], ncols: int) -> list[str]:
    "Glue the rows back together in groups of ncols"
    columns = [lines[i::ncols] for i in range(ncols)]
    return [",".join(row) + "\n" for row in zip(*columns)]


def repair_header(lines: list[str]) -> list[str]:
    "Return the table caption and column headers"
    description = lines[0].replace(",", "")
    raw_header = " ".join(lines[1:])
    raw_header = re.sub(
        r"[\.|]", "", raw_header
    )  # Clean up errant "." and "|" from OCR
    standard_headers = [  # Potential return values with standardised capitalisation
        "Temperature",
        "Measured Voltage",
        "Superconducting Signal",
    ]
    header = ",".join(
        list(filter(lambda h: h.lower() in raw_header.lower(), standard_headers))
    )
    return [f"{description}\n", f"{header}\n"]


def very_different(a: float, b: float) -> bool:
    "Arbitrarily decide if two values are very different"
    try:
        return abs(math.log10(abs(a / b))) > 3
    except ZeroDivisionError:
        return True


def close_enough(a: float, b: float) -> bool:
    "Arbitrarily decide if two values are close"
    try:
        return abs(math.log10(abs(a / b))) < 1
    except ZeroDivisionError:
        return True


def find_dislocation(lines: list[str], ncols: int) -> bool:
    """Look for occurences of a cell randomly moved to the wrong location as a
    result of "PDF ordering schenanigans". Assume the signature of this is when
    multiple cells in a row show large differences from their neighbours in the
    same column. Also assume that the first row is fine.
    A dislocation should exhibit a start point and an end point.
    This can be fixed by moving the cell from the end point to the start point.
    """
    start = -1
    end = -1
    for i, line in enumerate(lines):
        if i < ncols or i >= len(lines) - ncols:
            continue  # Skip first row
        if start == -1:
            if very_different(float(line), float(lines[i - ncols])):
                start = i
                if DEBUG:
                    print(f"found dislocation start: {i} ({line})")
        else:
            if close_enough(float(line), float(lines[i + 1])):
                end = i + 1
                if DEBUG:
                    print(f"found dislocation end: {i} ({line})")
                cell = copy(lines[end])
                del lines[end]
                lines.insert(start, cell)
                return True
    return False


def find_outliers(lines: list[str], ncols: int):
    """Look for occurences of a single cell in a row with a large difference
    from its neighbours in the same column. Assume that outliers never occur in
    multiple consecutive rows, that only one outlier appears per row, and that
    the first and last row are fine.
    This is likely an OCR error, and the row should just be discarded.
    """
    outliers = []
    rows = repair_rows(lines, ncols)
    for i, _ in enumerate(rows):
        if i == 0 or i == len(rows) - 1:
            continue
        prev, row, next = [
            [float(x) for x in rows[i + x].split(",")] for x in [-1, 0, 1]
        ]
        num_different = sum(
            [
                very_different(cell, prev[j]) and very_different(cell, next[j])
                for j, cell in enumerate(row)
            ]
        )
        num_same = sum(
            [
                close_enough(cell, prev[j]) and close_enough(cell, next[j])
                for j, cell in enumerate(row)
            ]
        )
        if num_different == 1 and num_same == len(row) - 1:
            if DEBUG:
                print(f"outlier found:\n\t{prev}\n\t{row}\n\t{next}", file=sys.stderr)
            outliers += [i * ncols + j for j in range(ncols)]
    return outliers


def main(filename: str):
    tables = [[]]
    headers = [[]]

    with open(filename, "r") as f:
        for l in f.readlines():
            line = l.strip()
            if "Table" in line:
                headers += [[line]]
                tables += [[]]
            elif len(line) > 1:
                try:
                    line = repair_float(line)
                    tables[-1] += [line]
                except ValueError:
                    if DEBUG:
                        print(
                            f"cannot convert to float: {line.strip()}", file=sys.stderr
                        )
                    headers[-1] += [line]

    for i, (header, table) in enumerate(zip(headers, tables)):
        if len(table) == 0:
            continue
        data_header = repair_header(header)
        ncols = len(data_header[1].split(","))
        for j in find_outliers(table, ncols)[::-1]:
            del table[j]
        j = 0
        while True:
            if not find_dislocation(table, ncols):
                break
            j += 1
            if DEBUG:
                print(f"fix dislocations: {j}")
            if j > 100:
                break
        data = repair_rows(table, ncols)
        if DEBUG:
            print(repair_header(header))
            print(f"{len(data)} rows")
        with open(f"data/table_{i:02}.csv", "w") as f:
            f.writelines(data_header + data)


if __name__ == "__main__":
    filename = sys.argv[1]
    main(filename)
