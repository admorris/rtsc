# Superconductivity data for Carbonaceous Sulfur Hydride

Data extraction and processing from:

- Dias, R. P. and Salamat, A., *Standard Superconductivity in Carbonaceous Sulfur Hydride* [arXiv:2111.15017](https://arxiv.org/abs/2111.15017) [cond-mat.supr-con] (2021)

For more information see the [documentation](doc/index.md).

## Dependencies

The following programmes are used to download and process the document:

- [Wget](https://www.gnu.org/software/wget/)
- [OCRmyPDF](https://ocrmypdf.readthedocs.io/en/latest/installation.html)
- `pdftotext` from [Poppler utils](https://poppler.freedesktop.org/)

The workflow is executed with [Make](https://www.gnu.org/software/make/), and the output data files are versioned with [GitPython](https://gitpython.readthedocs.io).

The extracted data is further processed with [Python](https://www.python.org/) using the following packages:

- [NumPy](https://numpy.org/)
- [pandas](https://pandas.pydata.org/)
- [Matplotlib](https://matplotlib.org/)

## Execution

For reproducibility, the workflow is captured in the `Makefile`.

To run the full analysis, execute the command:
```
make all -j
```

To just run the data extraction, execute the command:
```
make data -j
```

## Personal interest

*__Disclaimer:__ I am not a condensed matter physicist, thus I am in no position to comment on superconductivity research.*

I am a postdoctoral researcher in experimental particle physics at the University of Bonn and a member of the LHCb Collaboration at CERN.
Part of my work is on [analysis preservation and open data](https://lhcb-dpa.web.cern.ch/lhcb-dpa/wp6/index.html), including the release of LHCb open data for the years 2011-2012 (as part of the wider [CERN Open Data](http://opendata.web.cern.ch/) effort) as well as developing software to improve its accessibility for non-members of the collaboration.

My primary aim with this code repository is to improve the accessibility and interoperability of the released superconductivity data for CSH by extracting it from the PDF document and converting to machine-readable files that can be more readily analysed by others.
See the [data extraction and processing](#data-extraction-and-processing) section for more details.

My secondary aims are to pursue my own amateur (and likely misguided) analysis of the data, in particular to investigate the visually-striking discontinuities in the 160 GPa magnetic susceptibility curve, reported as evidence of fraud by van der Marel and Hirsch.
