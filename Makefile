FILEBASE=2111.15017
PAGES="70-149"
PRESSURES=138 166 178 189 182 160
EXT=png
DATA=$(patsubst %, data/%.csv, $(PRESSURES))
OUTPUT=$(patsubst %, plots/%_mv_ss.$(EXT), $(PRESSURES))
DCHI=$(patsubst %, plots/%_dchi.$(EXT), $(PRESSURES))

.PHONY:
all: $(OUTPUT) plots/160_dchi.$(EXT)

.PHONY:
clean:
	rm -f {plots,data}/*

.PHONY:
data: $(DATA)

.PHONY:
paper: doc/paper.pdf

doc/%.pdf: doc/%.md doc/header.tex doc/bibliography.bib $(OUTPUT) plots/160_dchi.$(EXT)
	@pandoc --citeproc -o $@ $< -H doc/header.tex -f "markdown+implicit_figures+auto_identifiers+yaml_metadata_block+smart+fenced_divs"

plots/%_mv_ss.$(EXT): scripts/plot.py data/%.csv
	@mkdir -p plots
	python $^ $@

plots/%_dchi.$(EXT): scripts/steps.py data/%.csv
	@mkdir -p plots
	python $^ $@

data/138.csv: scripts/join.py data/table_01.csv data/table_07.csv
	python $^ > $@

data/166.csv: scripts/join.py data/table_02.csv data/table_08.csv
	python $^ > $@

data/178.csv: scripts/join.py data/table_03.csv data/table_09.csv
	python $^ > $@

data/189.csv: scripts/join.py data/table_04.csv data/table_10.csv
	python $^ > $@

data/160.csv: scripts/join.py data/table_05.csv
	python $^ > $@

data/182.csv: scripts/join.py data/table_06.csv
	python $^ > $@

data/table_%.csv: scripts/split.py $(FILEBASE)_ocr.txt
	@mkdir -p data
	python $^

$(FILEBASE).pdf:
	@echo "Downloading $@"
	@wget --user-agent="Mozilla" https://arxiv.org/pdf/$@ # Spoof the user agent because arXiv doesn't like wget

%_ocr.pdf: %.pdf
	@echo "Running OCR"
	@ocrmypdf --pages $(PAGES) $< $@

%.txt: %.pdf
	@echo "Converting to plaintext"
	@pdftotext -f 12 $<
	@sed -r -i 's/\x0C//g' $@ # Remove random "Form Feed" characters
	@sed -r -i 's/§/5/g' $@ # The OCR sometimes interprets '5' as '§'
	@sed -r -i 's/~/-/g' $@ # ... and '-' as '~'
