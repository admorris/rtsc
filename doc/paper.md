---
title: "Extraction and analysis of superconductivity data for Carbonaceous Sulfur Hydride"
author:
  - "Adam Morris, University of Bonn"
date: \today
csl: "https://raw.githubusercontent.com/citation-style-language/styles/master/dependent/physics-letters-b.csl"
bibliography: "doc/bibliography.bib"
classoption: twocolumn
---


# Background

In October 2020, a paper reporting the observation of room-temperature superconductivity (RTSC) in carbonaceous sulfur hydride (CSH) was published in *Nature*.[@nature-rtsc]
To quote the abstract:

> Here we report superconductivity in a photochemically transformed carbonaceous sulfur hydride system, starting from elemental precursors, with a maximum superconducting transition temperature of 287.7±1.2 kelvin (about 15 degrees Celsius) achieved at 267±10 gigapascals.
> The superconducting state is observed over a broad pressure range in the diamond anvil cell, from 140 to 275 gigapascals, with a sharp upturn in transition temperature above 220 gigapascals.

Jorge Hirsch (he of [*h*-index](https://en.wikipedia.org/wiki/H-index) fame) is a vocal critic of this---and similar---work on superconductivity in hydrides, producing a number of papers and pre-prints on the matter over the course of 2021 alone.[@hirsch-210825; @hirsch-210421; @hirsch-210515; @hirsch-210907; @hirsch-210922]
To paraphrase the news article in *Science*,[@science-mag] one particular concern is over the reluctance of the authors of the October 2020 RTSC in CSH paper[@nature-rtsc] to release their raw data.

In November 2021, Dias and Salamat submitted a pre-print addressing this criticism,[@arxiv-data] which included raw data for the magnetic susceptibility measurements over a pressure range between 138 and 189 GPa.

In January 2022, Hirsch and Dirk van der Marel submitted a pre-print reporting an analysis of the magnetic susceptibility curve at 160 GPa, which contains discrete discontinuities that the authors interpret as data manipulation.
A XSLX spreadsheet of part of the 160 GPa data was made available along with the pre-print.[@spreadsheet]

# Data extraction and processing

Extracting data from tables embedded in a PDF document (whether by hand or with a script) is tedious and prone to errors.
I encourage the authors to upload this data in a machine-readable format (preferably CSV) to an appropriate platform, *e.g.* [Zenodo](https://zenodo.org/).

Six datasets (at pressures of 138, 160, 166, 178, 182 and 189 GPa with columns labelled "temperature", "measured voltage" and "superconducting signal") are provided as 10 tables embedded in a PDF document.[@arxiv-data]
The tables are captioned Table 1, 2, 3, 4, 5, 6, 7, 7, 7 and 7 (*sic*).
Tables 1--4 contain temperature v measured voltage data at pressures of 138, 166, 178 and 189 GPa, respectively, while the corresponding "superconducting signal" columns are found in Tables 7--7, presented in the same increasing order of pressure.
Tables 5 and 6 contain all three columns for pressures of 160 and 182 GPa.

Within the PDF document, Tables 1--4 are embedded in a way that the text is copiable, however the other six are full-page raster-graphic images (likely JPEGs), including noticeable compression artifacts.

The steps I took to extract the tables are as follows:

1. Use optical character recognition (OCR) software to overlay plaintext versions of the last six tables in the PDF document.
2. Extract the plaintext data from the pages containing the tables in the PDF document with the OCR overlay.
3. Identify and correct OCR mistakes where possible.
4. Identify and correct extreme outliers (arising from remaining uncorrected OCR mistakes) and move cells displaced in the PDF-to-plaintext conversion back to their intended position.
5. Write the tabulated data to CSV files
6. Join the "superconducting signal" data from Tables 7--7 onto Tables 1--4.

## Optical character recognition

I used the program [OCRmyPDF](https://ocrmypdf.readthedocs.io/) (itself based on the [Tesseract](https://tesseract-ocr.github.io/) OCR engine) to overlay plaintext versions of pages 70--149 of the PDF document.
Due to the initial poor quality (namely the low resolution and presence of compression artifacts), I encountered several recurring mistakes in the output:

- The number 5 interpreted as the section symbol: `5` → `§`
- Hyphen (proxy for a minus sign) interpreted as tilde: `-` → `~`
- Table borders interpreted as pipe characters: `|`
- The string `E-08` frequently interpreted as `E-03` or `E-038`
- Insertion of spurious spaces due to bad kerning
- Commas or full stops appended to the end of numbers
- Misplaced decimal points (likely due to bad kerning or compression artifacts)

With the exception of misplaced decimal points and omitted minus signs, the above errors were corrected with straightforward substitutions using `sed` and the python `re.sub` function.
This proved sufficient to ensure that all numbers could be cast from the python builtin `str` to `float`.

## Plaintext extraction, cleanup and conversion

To extract the text, I ran `pdftotext` from [Poppler utils](https://poppler.freedesktop.org/) starting at Table 5 on page 12.
The output was then cleaned up and converted to CSV files using the python script `split.py`.

The output from `pdftotext` consisted of numbers and snippets of text separated by multiple linebreaks.
These are processed line-by-line into arrays of strings.
Lines starting with `Table` (from the table captions) are treated as the delimiter that triggers the filling of a new array.
Non-empty lines which successfully convert to a `float` are treated as numerical data, otherwise as column header text.

Extreme outlying datapoints resulting from misplaced decimal points in the OCR output (see the [previous section](#optical-character-recognition)) are detected by looking for rows where a single cell is more than several orders of magnitude different from both neighbours in the same column. These rows are simply deleted.
Outliers arising from omitted minus signs are currently not handled at this step.

The order of the extracted text is unfortunately not guaranteed.
The [documentation for OCRmyPDF](https://ocrmypdf.readthedocs.io/en/latest/introduction.html) describes the challenge of extracting text from a PDF quite succinctly:

> PDF encodes the position of text glyphs but does not encode document structure.
> There is no markup that divides a document in sections, paragraphs, sentences, or even words (since blank spaces are not represented).
> As such all elements of document structure including the spaces between words must be derived heuristically.
> Some PDF viewers do a better job of this than others.

Fortunately, the left-to-right top-to-bottom reading order of the data was mostly correctly preserved, however there were a few instances of cells appearing in the wrong position.
These were handled with the function `find_dislocation`, which relies on the facts that values from each column are identifiable by their order of magnitude, and that the difference between neighbouring points from the same column is "small".
The start of a "dislocation" (*i.e.* the displaced cell should have been) is detected by looking for the first cell whose previous neighbour in the same column is of a very different order of magnitude.
The end of the "dislocation" (*i.e.* the displaced cell itself) is detected by looking for two cells in a row that are close in value.
The value at the "end" index is then moved to the "start" index within the array.

Since four of the six datasets were spread across two tables, the final step is to merge them with the script `join.py`, which uses the function [`pandas.DataFrame.merge`](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.merge.html).
Finally, any remaining significant outliers (mainly arising from missing minus signs) are detected by evaluating a rolling [Z-score](https://en.wikipedia.org/wiki/Standard_score) over a window of 30 points. Rows are removed if any cell has a Z-score greater than 3.

# Analysis

I started by replicating the analysis performed by van der Marel and Hirsch of the 160 GPa magnetic susceptibility data.
The first-order "discrete derivative" (each point replaced by its difference from the previous point) indeed shows distinct well-separated "bands".
Furthermore, it appears that each band follows the same shape, indicating that the susceptibility curve is the sum of a "digital" and a "smooth" component.
I will not go as far as saying that this constitutes evidence of data manipulation, but it is a curiousity that begs explanation.
Rather than adjust the data by hand (as indicated in the documentation of the spreadsheet[@spreadsheet]) to recover the "smooth" component, I flattened the curves by adding 0.5, converting to an integer with floor-rounding and then taking the modulo with 1.
(In reality, the shift of 0.5 had to be adjusted slightly to account for the amplitude of the "smooth" curve.)
Performing the integral of this distribution extracts the "digital" component from the signal, and the "smooth" component is what remains after the "digital" component is subtracted.

\Begin{widefig}

![Measured voltage (top left) and background-subtracted magnetic susceptibility (top right) curves as a function of temperature at 160 GPa. Also plotted are the first "discrete derivatives" (middle row) and the absolute value of the second "discrete derivatives" (bottom row).](plots/160_mv_ss.png){width=100%}

\End{widefig}

Following on from this, I looked at trying to characterise the "smooth" component, as its features appear mathematically simple.
A polynomial curve fit was unable to satisfactorally describe the curve.
A univariate cubic-spline fit was able to closely approximate the curve using 18 spline knots, the positions of which are shown as vertical dashed lines.
The caption of figure 2 in the Nature paper[@nature-rtsc] mentions background subtraction using data collected at 108 GPa, and I speculate that this may have been smoothed with splines first.

![The "digital" (top plot) and "smooth" (middle plot) components extracted from the magnetic susceptibility data. The bottom plot shows a univariate spline fit overlayed onto the "smooth" component, with vertical lines denoting the positions of spline knots.](plots/160_dchi.png){width=50%}

The second-order "discrete derivative" of the susceptibility curve suppresses the "smooth" component, leaving marked horizontal bands.
By binning the distribution of the second "discrete derivative" in a histogram, the step size (*i.e.* resolution) of the "digital" component can be estimated by finding the spacing between the peaks.
I performed this exercise for all available datasets, and each one demonstrates visible horizontal bands but with varying spacing.
Since the spacings seen at other pressures are much smaller than the amplitude of the underlying noise and "smooth" component (if present), I did not attempt to extract the same components.

![$|\Delta^2\chi|$ at 138 GPa](plots/138_mv_ss_ddchi_histogram.png){width=50%}

![$|\Delta^2\chi|$ at 160 GPa](plots/160_mv_ss_ddchi_histogram.png){width=50%}

![$|\Delta^2\chi|$ at 166 GPa](plots/166_mv_ss_ddchi_histogram.png){width=50%}

![$|\Delta^2\chi|$ at 178 GPa](plots/178_mv_ss_ddchi_histogram.png){width=50%}

![$|\Delta^2\chi|$ at 182 GPa](plots/182_mv_ss_ddchi_histogram.png){width=50%}

![$|\Delta^2\chi|$ at 189 GPa](plots/189_mv_ss_ddchi_histogram.png){width=50%}

Looking at the measured voltage data, it is evident from the first-order "discrete derivative" that the variance is much larger than in the magnetic susceptibility data.
This could be explained if it were simply the voltage measured over pickup coil, thus lacking the common-background subtraction provided by the dummy coil.
From the second-order "discrete derivative", there is no apparent "digital" component.

# Conclusion

The magnetic susceptibility data provided by Dias and Salamat[@arxiv-data] appears to contain a discretised/digital component at all pressures, albeit with varying step size/resolution.
This was previously noticed by by van der Marel and Hirsch in the 160 GPa dataset[@hirsch-2201], which has the largest step size.
Using a similar technique, I extracted both the "smooth" component as well as the "digital" component from the 160 GPa data, but the smaller step size in the other data presents a challenge.
Such a "digital" component is not evident in the measured voltage data, which suggests that this effect somehow results from the process of background subtraction.

# Reproducibility

The data-extraction and analysis workflow is defined in the `Makefile` contained in the repository.
Since the data was released without an explicit licence, I did not feel comfortable simply distributing the output, therefore I am distributing the means to obtain it.
This should also enforce the notion that the extracted data does not 100% faithfully represent the original data.

The data files produced by this repository are versioned (with git tag if available, otherwise the commit sha) for the sake of data provenance.
Since the code to extract the data may be improved in the future, I urge anyone using this code repository to pay attention to this versioning and make a note of it when presenting results.

# Bibliography

\vskip1em
